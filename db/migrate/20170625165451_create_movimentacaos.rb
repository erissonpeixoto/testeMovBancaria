class CreateMovimentacaos < ActiveRecord::Migration[5.0]
  def change
    create_table :movimentacaos do |t|
      t.references :conta, foreign_key: true
      t.date :data_movimentacao
      t.boolean :tranferencia
      t.boolean :saque
      t.boolean :deposito

      t.timestamps
    end
  end
end
