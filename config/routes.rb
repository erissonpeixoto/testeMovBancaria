Rails.application.routes.draw do
  
  devise_for :users
  resources :movimentacaos
  resources :conta

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
  #pagina inicial
  root :to => "home#index"

  resources :conta do
       member do
  #       get 'short'
         put 'inativa'
       end
  #
  #     collection do
  #       get 'sold'
  #     end
  end
  
end
