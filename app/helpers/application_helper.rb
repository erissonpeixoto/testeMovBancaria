module ApplicationHelper

	#metodo usado para marcar os links do navbar como ativo
	def active_class(link_path)
  		#current_page?(link_path) ? "active" : ""
  		current_route = Rails.application.routes.recognize_path(link_path)
    	"active" if current_page?(link_path) or params[:controller] == current_route[:controller]
    	
 	end
 	
end
