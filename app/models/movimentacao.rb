class Movimentacao < ApplicationRecord
  belongs_to :conta, optional: true, class_name: Contum, foreign_key: :conta_id

  belongs_to :conta_receptora, optional: true, :class_name => "Contum"

  validates_presence_of :conta, :data_movimentacao, :valor

  def trata_movimentacao
  	#byebug
    if self.conta.nil?
        errors.add(:conta, :blank, message: "Informe a conta de origem!")
        return false
    end
  	if self.valor.to_int < 0
  		#byebug
  		errors.add(:valor, :blank, message: "não pode ser negativo!")
  		return false
  	end	

    @conta_origem = Contum.find_by(id: self.conta_id)
    #byebug
  	if self.tranferencia #errei a ortografia quando criei. ->transferencia#
      #byebug
      if self.conta_receptora_id.nil?
        errors.add(:conta_receptora_id, :blank, message: "Informe a conta para transferência!")
        return false
      end
  		
      @valor_total_debitado = @conta_origem.saldo - self.valor
      if self.data_movimentacao.wday >= 1 && self.data_movimentacao.wday <= 5
         if Time.current.hour >= 9 && Time.current.hour <= 18
            @valor_total_debitado = @valor_total_debitado - 5 #taxa de transferencia
         else
            @valor_total_debitado = @valor_total_debitado - 7 #taxa de transferencia 
         end       
      else
          @valor_total_debitado = @valor_total_debitado - 7 #taxa de transferencia
      end

      if self.valor > 1000
         @valor_total_debitado = @valor_total_debitado - 10 #taxa adicional
      end 

  		if not @conta_origem.update_columns(:saldo => @valor_total_debitado)
  			errors.add(:valor, :blank, message: "Não foi possível fazer transferência!")
  			return false
  		end
      @valor_total_creditado = self.conta_receptora.saldo + self.valor	
      if not self.conta_receptora.update_columns(:saldo => @valor_total_creditado)
        errors.add(:valor, :blank, message: "Não foi possível fazer transferência!")
        return false
      end
  	end

    if self.saque
      @valor_total = @conta_origem.saldo - self.valor
      #byebug
      if @conta_origem.saldo < self.valor
        errors.add(:valor, :blank, message: "Saldo não pode ficar negativo!")
        return false
      end
      if not @conta_origem.update_columns(:saldo => @valor_total)
        errors.add(:valor, :blank, message: "Não foi possível fazer o saque!")
        return false
      end 
    end

    if self.deposito
    #  if self.data_movimentacao.wday > 5  
    #    errors.add(:valor, :blank, message: "Depósito só é possível em dias úteis!")
    #    return false
    #  end
      @valor_total_deposito = @conta_origem.saldo + self.valor 
      if not @conta_origem.update_columns(:saldo => @valor_total_deposito)
        errors.add(:valor, :blank, message: "Não foi possível fazer o deposito!")
        return false
      end 
    end

  	return true	
  end
end
