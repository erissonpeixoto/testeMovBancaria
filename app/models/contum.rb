class Contum < ApplicationRecord
	has_many :movimentacao, :dependent => :restrict_with_error 

	validates_presence_of :banco, :conta_corrent, :agencia, :saldo
      
	#para mostrar no selectbox
	def full_name
   		"#{banco} - CC #{conta_corrent} - Ag #{agencia}"
	end

	def set_inativo
    	self.ativo = false
  	end

end
