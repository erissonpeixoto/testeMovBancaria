json.extract! movimentacao, :id, :conta_id, :data_movimentacao, :tranferencia, :saque, :deposito, :created_at, :updated_at
json.url movimentacao_url(movimentacao, format: :json)
