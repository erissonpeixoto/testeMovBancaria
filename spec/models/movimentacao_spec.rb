require 'rails_helper'

RSpec.describe Movimentacao, type: :model do
  #pending "add some examples to (or delete) #{__FILE__}"

  ###Testes abaixo são para o trata_movimentacao

  it "é invalido sem a conta origem" do 
  	mov = Movimentacao.new(conta: nil) 
  	mov.trata_movimentacao  
  	expect(mov.errors[:conta]).to include("Informe a conta de origem!") 
  end

  it "valor não pode ser negativo" do
  	@conta = Contum.new(id: 1) 
  	mov = Movimentacao.new(conta: @conta, valor: -1) 
  	mov.trata_movimentacao  
  	expect(mov.errors[:valor]).to include("não pode ser negativo!") 
  end

  it "se transferência, Informe a conta receptora" do
  	@conta = Contum.new(id: 1) 
  	mov = Movimentacao.new(conta: @conta, valor: 1, tranferencia: true) 
  	mov.trata_movimentacao  
  	expect(mov.errors[:conta_receptora_id]).to include("Informe a conta para transferência!") 
  end

  # it "se saque, saldo não pode ficar negativo" do
  # 	@conta = Contum.new(id: 2, saldo: 10) 
  # 	mov = Movimentacao.new(conta: @conta, valor: 1, saque: true, valor: 15) 
  # 	mov.trata_movimentacao  
  # 	expect(mov.errors[:valor]).to include("Saldo não pode ficar negativo!") 
  # end

end
